// указываем адрес и порт хоста:
var host = "ws://192.168.1.23:8888";
// считываем имя пользователя 
var userName = document.getElementById("userName").value;
// получаем ссылки на необходимые нам DOM-элементы:
// поле ввода, кнопки, список сообщений
var msg = document.getElementById("message");
var sendBtn = document.getElementById("sendButton");
var msgList = document.getElementById("messageList");
// Проверяем, поддерживает ли браузер веб-сокеты
if (typeof (WebSocket) == "undefined") {
	// если не поддерживает - сообщаем об этом пользователю
	alert("Ваш браузер не поддерживает websockets. Попробуйте использовать Chrome или Safari.");
} else {
	// иначе - создаем соединение с сервером,
	// указывая в качестве параметра его адрес и порт.
	// Обратите внимание на то, что используется протокол "ws" (сокращенно от
	// WebSocket).
	var ws = new WebSocket(host);
	// создаем обработчик нажатия кнопки, по нажатию на которую,
	// мы отправим сообщение на сервер и очистим поле для ввода сообщений
	sendBtn.onclick = function() {
		if (msg.value.length < 200) {
			ws.send(userName + ": " + msg.value);
			msg.value = "";
			sendBtn.disabled = true;
			window.setTimeout('sendBtn.disabled=0', 15000);
		} else {
			msgList.innerHTML = '<span style="color:red">Сообщение не отправлено: Размер превысил допустимые значения</span><hr />' + msgList.innerHTML;
		}
	}
	// создаем обработчик события "onmessage",
	// которое сработает, когда сервер пришлёт нам сообщение
	ws.onmessage = function(event) {
		// добавляем пришедшее сообщение в список
		msgList.innerHTML = event.data + "<hr />" + msgList.innerHTML;
	}
}