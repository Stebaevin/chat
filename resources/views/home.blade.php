
@extends('app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			<div class="panel panel-default">
				<div class="panel-heading">Home</div>

				<div class="panel-body">
					
					<form name="form" class="form-horizontal" role="form" method="POST" action="{{ url('/home') }}" onSubmit="return false;">
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
						<input type="hidden" id="userName" value="{{Auth::user()->name}}" /> 
						
						<!-- Поле для ввода сообщений --> 
						<input type="text" class="span4" id="message" /> 
						
						<!-- Кнопка для отправки сообщений --> 
						<input type="button" class="btn" id="sendButton" value="отправить" /> 
						@if (Auth::user()->admin == true)
							<hr /> 
							<br>Введите имя пользователя: 					
							<input type="text" class="span2" id="nameBlockUser" name="nameBlockUser"/>  
							<input type="button" class="btn" id="ButtonBlockUser" value="сменить статус" title="Отключить/включить пользователя" onclick="divToTextarea()"/> 
						@endif
						<textarea id="textMessageList" name="textMessageList" style="display:none;"></textarea>
					</form>

					<hr /> 
					<!-- Область для вывода всех сообщений --> 
					<div id="messageList"><?php if (isset($message)) echo $message; ?></div> 

				</div>
			</div>
		</div>
	</div>
</div>

<!-- Подключаем скрипт, реализующий клиентскую часть функционала --> 
<script src="{{ asset('/public/js/client.js') }}"></script>

<script>
function divToTextarea() {
	document.getElementById("textMessageList").value = document.getElementById("messageList").innerHTML;
	window.document.forms['form'].submit();}
</script>

@endsection
