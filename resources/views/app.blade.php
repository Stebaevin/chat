<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>TstlgnLaravel</title>

	<link href="{{ asset('/public/css/app.css') }}" rel="stylesheet">
    <LINK href="{{ asset('/public/css/Opersin.css') }}" type="text/css" rel="stylesheet"> 
    <link href="{{ asset('/public/css/bootstrap.css') }}" rel="stylesheet">
    <link href="{{ asset('/public/css/bootstrap-responsive.css') }}" rel="stylesheet">

    <style type="text/css">
      body {
        padding-top: 20px;
        padding-bottom: 60px;
      }

      /* Custom container */
      .container {
        margin: 0 auto;
        max-width: 1000px;
      }
      .container > hr {
        margin: 60px 0;
      }

      /* Main marketing message and sign up button */
      .jumbotron {
        margin: 80px 0;
        text-align: center;
      }
      .jumbotron h1 {
        font-size: 100px;
        line-height: 1;
      }
      .jumbotron .lead {
        font-size: 24px;
        line-height: 1.25;
      }
      .jumbotron .btn {
        font-size: 21px;
        padding: 14px 24px;
      }

      /* Supporting marketing content */
      .marketing {
        margin: 60px 0;
      }
      .marketing p + h4 {
        margin-top: 28px;
      }


      /* Customize the navbar links to be fill the entire space of the .navbar */
      .navbar .navbar-inner {
        padding: 0;
      }
      .navbar .nav {
        margin: 0;
      }
      .navbar .nav li {
        display: table-cell;
        width: 1%;
        float: none;
      }
      .navbar .nav li a {
        font-weight: bold;
        text-align: center;
        border-left: 1px solid rgba(255,255,255,.75);
        border-right: 1px solid rgba(0,0,0,.1);
      }
      .navbar .nav li:first-child a {
        border-left: 0;
        border-radius: 3px 0 0 3px;
      }
      .navbar .nav li:last-child a {
        border-right: 0;
        border-radius: 0 3px 3px 0;
      }
    </style>

    <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!-- Fav and touch icons -->
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="{{ asset('/public/ico/apple-touch-icon-144-precomposed.png') }}">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="{{ asset('/public/ico/apple-touch-icon-114-precomposed.png') }}">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="{{ asset('/public/ico/apple-touch-icon-72-precomposed.png') }}">
    <link rel="apple-touch-icon-precomposed" href="{{ asset('/public/ico/apple-touch-icon-57-precomposed.png') }}">
    <link rel="shortcut icon" href="{{ asset('/public/ico/favicon.png') }}">

	<!-- Fonts -->
	<!-- заком.Стебаев <link href='//fonts.googleapis.com/css?family=Roboto:400,300' rel='stylesheet' type='text/css'> -->

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
</head>

<body>
	<div class="container" id="logo">
		<div id="logotip">
		    <img height="100" width="108" src="{{ asset('/public/image/Smile.jpg') }}" alt="logotip" />
		</div>
		<div id="zagolovok">
		    <h2>Чат</h2>
		</div>
		<div id="telefon">
		    <p>Контактные телефоны</p>
		    <table id="tel">
		    	<tr>
		        	<td><img height="35" width="47" src="{{ asset('/public/image/kiyvstar.jpg') }}" alt="Kyivstar" /></td>
		        	<td> +380675711605</td>
			    </tr>
		    </table>
		</div>
	</div>

	<nav class="navbar">
		<div class="container-fluid">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
					<span class="sr-only">Toggle Navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
			</div>

			<div class="navbar" id="bs-example-navbar-collapse-1">
				<ul class="nav">
					<li><a href="{{ url('/') }}">Home</a></li>

					@if (Auth::guest())
						<li><a href="{{ url('/auth/login') }}">Вход</a></li>
						<li><a href="{{ url('/auth/register') }}">Регистрация</a></li>
					@else
								<li>
									<a href="#" role="button" aria-expanded="false">{{ Auth::user()->name }} </a>
								</li>
								<li><a href="{{ url('/profile') }}">Изменить настройки</a></li>
								<li><a href="{{ url('/auth/logout') }}">Выход</a></li>
							</ul>
					@endif
				</ul>
			</div>
		</div>
	</nav>

	@yield('content')
 
    <script src="{{ asset('/public/js/jquery.js') }}"></script>
    <script src="{{ asset('/public/js/bootstrap.js') }}"></script>

</body>
</html>
