<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use Auth; // для идентификации
use App\User; // для update
use Illuminate\Http\Response;
//use App\Http\Controllers\Auth\AuthController;

class ProfileController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		// запускается по GET url('/profile')
		return view('profile/update');

	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		//  запускается по POST url('/profile')
	
		if (isset($request) && ($request != null)) {

			// валидация:
			$this->validate($request, [
		        'name' => 'required|min:3|max:55|alpha_dash',
		        'password' => 'confirmed',
		    ], [
		    	'required' => 'Поле должно быть заполнено!']);

			// обработка валидации
			//if ($validator->fails()) {
			//    return Redirect::to('home')->withErrors($validator);
  			//}

			// получаем данные запроса
			$id       = $request->input('id');
			$name     = $request->input('name');
			$email    = $request->input('email');
			$password = $request->input('password');

			// записываем в базу
			$user = User::find($id);
			$user->name = $name;
			$user->email = $email;
			if ($password !== '') $user->password = bcrypt($password);
			$user->save();

			return view('home');
			//return view('profile/update');

		} else {
			return view('profile/update');
		}	
		
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	// изменение данных пользователя
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
