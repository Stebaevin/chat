<?php namespace App\Http\Controllers;

use App\User;
use Auth;
use Validator;
use Illuminate\Http\Request; 
//use Illuminate\Routing\Controller;

class HomeController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Home Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders your application's "dashboard" for users that
	| are authenticated. Of course, you are free to change or remove the
	| controller as you wish. It is just here to get your app started!
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('auth');
	}

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function index()
	{
		return view('home');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		//  запускается по POST url('/home')
	
		if (isset($request) && ($request != null)) {

			// валидация:
			$validator = Validator::make( [
		        'nameBlockUser' => 'required|min:3|max:55',
		    ], [
		    	'required' => 'Поле должно быть заполнено!']);

			// обработка валидации
			if ($validator->fails()) {
			    // return Redirect::to('home')->withErrors($validator);
				return view('home', ['message' => $request->input('textMessageList')]);
			}

			// получаем данные запроса
			$name = $request->input('nameBlockUser');
			$textMessageList = $request->input('textMessageList');
			// записываем в базу
			$user = User::where('name', $name)->first();
			
			$message = '';
			
			if ($user && (Auth::user()->admin == true) && ($user->admin == false)) {
				$user->blocked = !$user->blocked;
				$user->save();
				$message = 'Пользователь '. $user->name . (($user->blocked)? ' заблокирован' : ' разблокирован') . "<hr />"; 
			} else {
				$message = 'Сменить статус пользователю ' . $name . ' не удалось.' . "<hr />";
			}

			$message = $message . $textMessageList;
			return view('home', ['message' => $message]);
			//return view('profile/update');

		}	
		
	}

}
