<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

/*
Route::get('/', function () {
    return view('welcome');
});
*/

Route::filter('blocked', function() {
    if(Auth::user() && Auth::user()->blocked == 1){
// 		return Redirect::to('home');
    	Auth::logout();
 		return Redirect::to('auth/login')->withErrors('Пользователь заблокирован');
    }
});

Route::get('/', array('before' => 'blocked', 'uses' => 'HomeController@index'));

Route::post('/', array('before' => 'blocked', 'uses' => 'HomeController@store'));

Route::get('home', array('before' => 'blocked', 'uses' => 'HomeController@index'));

Route::post('home', array('before' => 'blocked', 'uses' => 'HomeController@store'));

//Route::resource('home', 'HomeController');

Route::resource('profile', 'ProfileController');

Route::controllers([
		'auth' => 'Auth\AuthController',
		'password' => 'Auth\PasswordController',
]);
