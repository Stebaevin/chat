// указываем номер порта
var portNumber = 8888;

// настройка MySQL
var host     = 'localhost';
var user     = 'root';
var password = '';
var database = 'chat';


var WebSocketServer = new require('ws');

// подключенные клиенты
var clients = {};
// цвета подключенных клиентов
var colors = {};

// выдает случайное число от min до max 
function randomInt(min, max) {
	var rand = min - 0.5 + Math.random() * (max - min +1)
	rand = Math.round(rand);
	return rand;
}

// WebSocket-сервер на порту portNumber
var webSocketServer = new WebSocketServer.Server({port: portNumber});

webSocketServer.on('connection', function(ws) {

	// создаем массив случайныз цветов:
	var arrColorMax = [randomInt(0, 256), randomInt(0, 256), randomInt(0, 256)];
	// хотя бы один из цветов должен быть темным:
	if ((arrColorMax[0] + arrColorMax[1] + arrColorMax[2]) > 512)
		arrColorMax[randomInt(1 , 3)] = randomInt(0, 128);

	var id = Math.random();
	clients[id] = ws;
	colors[id] = arrColorMax[0].toString() + ',' + arrColorMax[1].toString() + ',' + arrColorMax[2].toString();
	console.log("новое соединение " + id);

	ws.on('message', function(message) {

		console.log('получено сообщение ' + message);

		// проверим на блокирование:
		var name = message.substring(0, message.indexOf(':'));

		// инициализация MySQL
		var mysql      = require('mysql');
		var connection = mysql.createConnection({
		  host     : host,
		  user     : user,
		  password : password,
		  database : database
		});

		connection.connect();
		connection.query('SELECT id from users where not blocked and name = "' + name +'"', function(err, rows, fields) {
			if (err) throw err;
			
			if (rows.length > 0) {
				console.log('отправляем всем сообщение от ' + name + ' id=', rows[0].id);
				// передаем сообщение всем
				for(var key in clients) {
					clients[key].send('<span style="color:rgb(' + colors[id] + ')">' + message + '</span>');
				}
			} else {
				console.log('Пользователь ' + name + ' не найден в числе активных участников');
				clients[id].send('<span style="color:rgb(256, 0, 0)">Ваш логин заблокирован. Обратитесь к администратору</span>');
			}
			connection.end();
		});



	});

	ws.on('close', function() {
		console.log('соединение закрыто ' + id);
		delete clients[id];
	});

});

console.log("Сервер запущен на порту " + portNumber);

