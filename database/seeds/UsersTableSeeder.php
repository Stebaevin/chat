<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		DB::table('users')->delete();

		App\User::create(array(
				'name' => 'admin',
				'email' => 'st_i@mail.ru',
				'password' => Hash::make('admin'),
				'admin' => true
		));
    }
}
